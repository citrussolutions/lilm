# LILM - Luovutusilmoitusasiakirja

Siemens PACS kuvantamisjärjestelmän tuottaman lokitiedoston pohjalta muodostetaan Kanta-palveluun tallennettava luovutusilmoitusasiakirja.

## TODO
- CDA:n allekirjoitus, testausta vailla
- ~~java-version nosto => 17~~
- ~~camel + springboot versionnosto uusimpiin~~
- ~~java-pakettien nimeäminen~~
- ~~tomcat käyttöön nykyisen palvelinsovelluksen sijaan~~
- mr_message.ftl receiver-arvo propseihin, ympäristökohtainen arvo
- kanta-apin kutsu
- ~~lokitus tiedostoon~~
- JUnit-testejä
- ~~skeemavalidointi, CDA + localheader + MR. Skeemat kanta.fi sivuilta~~
- sovellus käynnistymään windows-servicenä
- ~~sql-luontilauseet versionhallintaan~~
- dokumentointi, asennusohjeet, konfigurointiohjeet jne

### Ohjeistusta työkaluihin
keytool -importkeystore -srckeystore testlilmwprivkey.pfx -srcstoretype pkcs12 -destkeystore testi.jks -deststoretype pkcs12