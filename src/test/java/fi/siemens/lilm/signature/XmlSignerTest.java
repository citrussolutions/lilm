package fi.siemens.lilm.signature;

import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Path;

public class XmlSignerTest {

    @Test
    void signTest() throws Exception {
        var cda = Files.readString(Path.of("src/main/java/fi/siemens/lilm/signature/cda.xml"));
        var signedCda = new XmlSigner().sign(cda);

        System.out.println(signedCda);
    }
}
