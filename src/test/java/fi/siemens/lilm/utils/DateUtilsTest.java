package fi.siemens.lilm.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DateUtilsTest {

    @Test
    void pacsTimeAsDateTimeTest() {
        var pacsTime = "2021-12-23T16:00:00";
        var time = DateUtils.getPacsTimeAsLocalDateTime(pacsTime);

        var dt = DateUtils.getLocalDateTimeAsDateTime(time);

        assertEquals("20211223160000", dt);
    }

    @Test
    void pacsTimeAsDisplayDateTimeTest() {
        var pacsTime = "2021-12-23T16:00:00";
        var time = DateUtils.getPacsTimeAsLocalDateTime(pacsTime);
        var dt = DateUtils.getLocalDateTimeAsDisplayDateTime(time);

        assertEquals("23.12.2021 16:00:00", dt);
    }

    @Test
    void signatureTimestampTest() {

        var dt = DateUtils.getCurrentDateTimeAsISO();

        assertEquals(25, dt.length());
    }

    @Test
    void signatureTimestampAsDisplayDateTimeTest() {
        var time = "2022-06-14T20:50:58+03:00";

        var dt = DateUtils.getSignatureTimestampAsId(time);

        assertEquals("2022.06.14.20.50.58", dt);
    }
}
