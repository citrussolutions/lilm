package fi.siemens.lilm.utils;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class OidGeneratorTest {

    @Test
    void createTest() {
        var generator = new OidGenerator("1.2.246.1000");

        LocalDate now = LocalDate.now();

        var startStr = "1.2.246.1000." + now.getYear() + "." + now.getMonthValue() + "." + now.getDayOfMonth() + ".";

        var oid = generator.create();

        assertTrue(oid.startsWith(startStr));
    }
}
