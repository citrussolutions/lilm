package fi.siemens.lilm.utils;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BirthTimeTest {

    @Test
    void resolveTest1() {
        var b = BirthTime.resolve("23.01.2016");
        assertEquals("20160123", b);
    }

    @Test
    void resolveTest2() {
        var b = BirthTime.resolve("02.01.2006");
        assertEquals("20060102", b);
    }

    @Test
    void resolveTest3() {
        var b = BirthTime.resolve("17.02.1977");
        assertEquals("19770217", b);
    }

    @Test
    void resolveTest4() {
        var b = BirthTime.resolve("30.05.1899");
        assertEquals("18990530", b);
    }
}
