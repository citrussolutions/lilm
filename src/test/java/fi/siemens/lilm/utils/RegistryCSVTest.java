package fi.siemens.lilm.utils;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class RegistryCSVTest {

    @Test
    void generateSQL() throws IOException {

        var sql = RegistryCSV.generateSQL();

        Files.write(Paths.get("./data/rekisterit.sql"), sql.getBytes(StandardCharsets.UTF_8));
    }
}
