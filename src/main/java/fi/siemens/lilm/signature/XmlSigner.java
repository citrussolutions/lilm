package fi.siemens.lilm.signature;

import fi.siemens.lilm.utils.LogUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;

import javax.xml.crypto.dsig.*;
import javax.xml.crypto.dsig.dom.DOMSignContext;
import javax.xml.crypto.dsig.keyinfo.KeyInfo;
import javax.xml.crypto.dsig.keyinfo.KeyInfoFactory;
import javax.xml.crypto.dsig.keyinfo.X509Data;
import javax.xml.crypto.dsig.spec.C14NMethodParameterSpec;
import javax.xml.crypto.dsig.spec.TransformParameterSpec;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.*;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.KeyStore.PrivateKeyEntry;
import java.security.cert.X509Certificate;
import java.util.*;

@Component
public class XmlSigner {

	private static Logger logger = LoggerFactory.getLogger(XmlSigner.class);

	static {
		System.setProperty("com.sun.org.apache.xml.internal.security.ignoreLineBreaks", "true");
	}

	@Value("${certificate-path}")
	private String certificatePath;

	@Value("${pkey-password}")
	private String password;

	public String sign(String cda) throws Exception {

		logger.debug(LogUtils.BEGIN);

		XMLSignatureFactory fac = getXMLSignatureFactory();
		XPathFactory xPathfactory = XPathFactory.newInstance();

		Document doc = instantiateDocumentToBeSigned(cda);

		XPath xpath = xPathfactory.newXPath();

		// retrieve structuredBody ID because it is used in the URI attribute of the
		// signature.
		XPathExpression exprStructuredBodyID = xpath
				.compile("//*[local-name()='ClinicalDocument']//*[local-name()='structuredBody']//@ID");

		String structuredBodyID = (String) exprStructuredBodyID.evaluate(doc, XPathConstants.STRING);

		// retrieve SignatureTimestamp ID because it is used in the URI attribute of the
		// signature.
		XPathExpression exprSignatureTimestampID = xpath.compile(
				"//*[local-name()='ClinicalDocument']//*[local-name()='signatureCollection']//*[local-name()='signatureTimestamp']//@ID");

		String signatureTimestampID = (String) exprSignatureTimestampID.evaluate(doc, XPathConstants.STRING);

		// retrieve hl7fi:signature Node
		XPathExpression exprHL7Signature = xpath.compile(
				"//*[local-name()='ClinicalDocument']//*[local-name()='localHeader']//*[local-name()='signatureCollection']//*[local-name()='signature']");

		Node parentNode = ((Node) exprHL7Signature.evaluate(doc, XPathConstants.NODE));
		Node insertionNode = ((Node) exprHL7Signature.evaluate(doc, XPathConstants.NODE)).getLastChild();

		// retrieve PrivateKey from Specified KeyStore.
		PrivateKeyEntry keyEntry = loadPKCS12KeyStoreAndGetSigningKeyEntry(certificatePath, password);

		String strExpression = "//*[local-name()='ClinicalDocument']//*[local-name()='structuredBody']";
		getNodeToSign(doc, xpath, strExpression);

		strExpression = "//*[local-name()='ClinicalDocument']//*[local-name()='signatureCollection']//*[local-name()='signatureTimestamp']";
		getNodeToSign(doc, xpath, strExpression);

		// Create the DOMSignContext by specifying the signing informations: Private
		// Key, parent node of the created signature, Where to insert the Signature.
		DOMSignContext dsc = new DOMSignContext(keyEntry.getPrivateKey(), parentNode, insertionNode);
		dsc.setDefaultNamespacePrefix("ds");

		// Create a CanonicalizationMethod which specify how the XML will be
		// canonicalized before signed.
		CanonicalizationMethod canonicalizationMethod = fac.newCanonicalizationMethod(CanonicalizationMethod.EXCLUSIVE,
				(C14NMethodParameterSpec) null);

		// Create a SignatureMethod which specify how the XML will be signed.
		SignatureMethod signatureMethod = fac.newSignatureMethod(SignatureMethod.RSA_SHA256, null);

		// Create an Array of Transform, add it one Transform which specify the
		// Signature ENVELOPED method.
		List<Transform> transformList = new ArrayList<Transform>(1);
		transformList.add(fac.newTransform(CanonicalizationMethod.EXCLUSIVE, (TransformParameterSpec) null));

		// Create a Reference which contain: An URI to the structuredBodyID, the Digest
		// Method and the Transform List which specify the Signature ENVELOPED method.
		Reference referenceSB = fac.newReference("#" + structuredBodyID, fac.newDigestMethod(DigestMethod.SHA256, null),
				transformList, null, null);

		Reference referenceST = fac.newReference("#" + signatureTimestampID,
				fac.newDigestMethod(DigestMethod.SHA256, null), transformList, null, null);

		List<Reference> referenceList = Collections.unmodifiableList(Arrays.asList(referenceSB, referenceST));

		// Create a SignedInfo with the pre-specified: Canonicalization Method,
		// Signature Method and List of References.
		SignedInfo si = fac.newSignedInfo(canonicalizationMethod, signatureMethod, referenceList);

		// Create a new KeyInfo and add it the Public Certificate.
		KeyInfo ki = getKeyInfoWithX509Data(keyEntry, fac);

		XMLSignature signature = getXMLSignature(fac, si, ki);
		signature.sign(dsc);

		var result = writeResultingDocument(doc);

		logger.debug(LogUtils.END);

		return result;
	}

	private static XMLSignature getXMLSignature(XMLSignatureFactory fac, SignedInfo si, KeyInfo ki) {

		logger.debug(LogUtils.BEGIN);

		// Creating a random UUID (Universally unique identifier).
		UUID uuid = UUID.randomUUID();
		String randomUUIDString = uuid.toString();

		// Create a new XML Signature with the pre-created : Signed Info & Key Info
		XMLSignature signature = fac.newXMLSignature(si, ki, null, randomUUIDString, null);

		logger.debug(LogUtils.END);

		return signature;
	}

	private static Element getNodeToSign(Document doc, XPath xpath, String strExpression)
			throws XPathExpressionException {

		logger.debug(LogUtils.BEGIN);

		// retrieve Node to be signed.
		XPathExpression exprNode = xpath.compile(strExpression);
		Element nodeToSign = (Element) exprNode.evaluate(doc, XPathConstants.NODE);
		// set ID attribute
		nodeToSign.setIdAttribute("ID", true);

		logger.debug(LogUtils.END);

		return nodeToSign;
	}

	private static XMLSignatureFactory getXMLSignatureFactory() {
		return XMLSignatureFactory.getInstance("DOM");
	}

	private static PrivateKeyEntry loadPKCS12KeyStoreAndGetSigningKeyEntry(String pkcs12CertificateFilePath,
			String password) throws Exception {

		logger.debug(LogUtils.BEGIN);

		KeyStore ks = KeyStore.getInstance("PKCS12");
		ks.load(new FileInputStream(pkcs12CertificateFilePath), password.toCharArray());

		var entry = (PrivateKeyEntry) ks.getEntry(ks.aliases().nextElement(),
				new KeyStore.PasswordProtection(password.toCharArray()));

		logger.debug(LogUtils.END);

		return entry;
	}

	private static KeyInfo getKeyInfoWithX509Data(PrivateKeyEntry keyEntry, XMLSignatureFactory fac) {

		logger.debug(LogUtils.BEGIN);

		X509Certificate cert = (X509Certificate) keyEntry.getCertificate();
		KeyInfoFactory kif = fac.getKeyInfoFactory();
		List<Object> x509Content = new ArrayList<>();
		// x509Content.add(cert.getSubjectX500Principal().getName());
		x509Content.add(cert);
		X509Data xd = kif.newX509Data(x509Content);

		var keyInfo = kif.newKeyInfo(Collections.singletonList(xd));

		logger.debug(LogUtils.END);

		return keyInfo;
	}

	private static Document instantiateDocumentToBeSigned(String xmlContent) throws Exception {

		logger.debug(LogUtils.BEGIN);

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);

		Document doc;

		try (StringReader strReader = new StringReader(xmlContent)) {
			InputSource is = new InputSource(strReader);
			doc = dbf.newDocumentBuilder().parse(is);
			// doc.normalizeDocument();
		}

		logger.debug(LogUtils.END);

		return doc;
	}

	private static String writeResultingDocument(Document doc) throws Exception {

		logger.debug(LogUtils.BEGIN);

		var os = new ByteArrayOutputStream();
		TransformerFactory tf = TransformerFactory.newInstance();
		Transformer trans = tf.newTransformer();
		trans.transform(new DOMSource(doc), new StreamResult(os));

		var result = os.toString(StandardCharsets.UTF_8);

		logger.debug(LogUtils.END);

		return result;
	}
}
