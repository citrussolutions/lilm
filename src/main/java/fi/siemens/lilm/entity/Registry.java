package fi.siemens.lilm.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Registry")
public class Registry {

    @Id
    @Column(name = "name")
    private String name;
    @Column(name = "service_provider_id")
    private String serviceProviderId;
    @Column(name = "service_provider_name")
    private String serviceProviderName;
    @Column(name = "registry_keeper_id")
    private String registryKeeperId;
    @Column(name = "registryKeeperName")
    private String registryKeeperName;
    @Column(name = "registry")
    private Integer registry;
    @Column(name = "registry_specifier")
    private String registrySpecifier;
    @Column(name = "registry_specifier_name")
    private String registrySpecifierName;
    @Column(name = "assigned_custodian_id")
    private String assignedCustodianId;
    @Column(name = "assigned_custodian_name")
    private String assignedCustodianName;
    @Column(name = "represented_organization_id")
    private String representedOrganizationId;
    @Column(name = "represented_organization_name")
    private String representedOrganizationName;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getServiceProviderId() {
        return serviceProviderId;
    }

    public void setServiceProviderId(String serviceProviderId) {
        this.serviceProviderId = serviceProviderId;
    }

    public String getServiceProviderName() {
        return serviceProviderName;
    }

    public void setServiceProviderName(String serviceProviderName) {
        this.serviceProviderName = serviceProviderName;
    }

    public String getRegistryKeeperId() {
        return registryKeeperId;
    }

    public void setRegistryKeeperId(String registryKeeperId) {
        this.registryKeeperId = registryKeeperId;
    }

    public String getRegistryKeeperName() {
        return registryKeeperName;
    }

    public void setRegistryKeeperName(String registryKeeperName) {
        this.registryKeeperName = registryKeeperName;
    }

    public Integer getRegistry() {
        return registry;
    }

    public void setRegistry(Integer registry) {
        this.registry = registry;
    }

    public String getRegistrySpecifier() {
        return registrySpecifier;
    }

    public void setRegistrySpecifier(String registrySpecifier) {
        this.registrySpecifier = registrySpecifier;
    }

    public String getRegistrySpecifierName() {
        return registrySpecifierName;
    }

    public void setRegistrySpecifierName(String registrySpecifierName) {
        this.registrySpecifierName = registrySpecifierName;
    }

    public String getAssignedCustodianId() {
        return assignedCustodianId;
    }

    public void setAssignedCustodianId(String assignedCustodianId) {
        this.assignedCustodianId = assignedCustodianId;
    }

    public String getAssignedCustodianName() {
        return assignedCustodianName;
    }

    public void setAssignedCustodianName(String assignedCustodianName) {
        this.assignedCustodianName = assignedCustodianName;
    }

    public String getRepresentedOrganizationId() {
        return representedOrganizationId;
    }

    public void setRepresentedOrganizationId(String representedOrganizationId) {
        this.representedOrganizationId = representedOrganizationId;
    }

    public String getRepresentedOrganizationName() {
        return representedOrganizationName;
    }

    public void setRepresentedOrganizationName(String representedOrganizationName) {
        this.representedOrganizationName = representedOrganizationName;
    }
}
