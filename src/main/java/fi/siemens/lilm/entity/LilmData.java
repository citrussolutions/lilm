package fi.siemens.lilm.entity;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Lilmdata")
public class LilmData implements Serializable {

	private static final long serialVersionUID = 1798515713830951016L;

	public enum Status {
		SAVED,
		INPROGRESS,
		COMPLETE,
		ERROR,
		SEND_ERROR
		// DIGITAL_SIGNATURE_ERROR,
		// CDA_SCHEMA_ERROR,
		// SIGNED_CDA_SCHEMA_ERROR,
		// SOAP_SCHEMA_ERROR
	}

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "patid")
	private String patientId;
	@Column(name = "patname")
	private String patientName;
	@Column(name = "userid")
	private String userId;
	@Column(name = "birthdate")
	private String birthDate;
	@Column(name = "stuid")
	private String stuId;

	@Column(name = "givenname")
	private String givenName;
	@Column(name = "familyname")
	private String familyName;

	@Column(name = "document_id")
	private String documentId;
	private String event;
	private String application;
	private LocalDateTime time;

	@Enumerated(EnumType.STRING)
	private Status status;
	private String message;

	@Column(name = "kanta_response_code")
	private String kantaResponseCode;

	// source organization
	//
	@Column(name = "src_org_unit")
	private String srcOrgUnit;

	@Column(name = "src_service_provider_id")
	private String srcServiceProviderId;
	@Column(name = "src_service_provider_name")
	private String srcServiceProviderName;
	@Column(name = "src_registry_keeper_id")
	private String srcRegistryKeeperId;
	@Column(name = "src_registry_keeper_name")
	private String srcRegistryKeeperName;
	@Column(name = "src_registry")
	private Integer srcRegistry;
	@Column(name = "src_registry_specifier")
	private String srcRegistrySpecifier;
	@Column(name = "src_registry_specifier_name")
	private String srcRegistrySpecifierName;

	@Column(name = "assigned_custodian_id")
	private String assignedCustodianId;
	@Column(name = "assigned_custodian_name")
	private String assignedCustodianName;
	@Column(name = "represented_organization_id")
	private String representedOrganizationId;
	@Column(name = "represented_organization_name")
	private String representedOrganizationName;

	// receiving organization
	//
	@Column(name = "dest_org_unit")
	private String destOrgUnit;
	@Column(name = "dest_service_provider_id")
	private String destServiceProviderId;
	@Column(name = "dest_service_provider_name")
	private String destServiceProviderName;
	@Column(name = "dest_registry_keeper_id")
	private String destRegistryKeeperId;
	@Column(name = "dest_registry_keeper_name")
	private String destRegistryKeeperName;

	// generated oid
	@Column(name = "lilm_doc_id")
	private String lilmDocId;

	@Column(name = "dest_registry")
	private Integer destRegistry;
	@Column(name = "dest_registry_specifier")
	private String destRegistrySpecifier;
	@Column(name = "dest_registry_specifier_name")
	private String destRegistrySpecifierName;

	@Column(name = "transfer_count")
	private int transferCount = 0;

	private LocalDateTime created;
	private LocalDateTime modified;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getPatientId() {
		return patientId;
	}

	public void setPatientId(String patid) {
		this.patientId = patid;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patname) {
		this.patientName = patname;
	}

	public String getGivenName() {
		return givenName;
	}

	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	public String getFamilyName() {
		return familyName;
	}

	public void setFamilyName(String familyName) {
		this.familyName = familyName;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userid) {
		this.userId = userid;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getStuId() {
		return stuId;
	}

	public void setStuId(String studid) {
		this.stuId = studid;
	}

	public String getDocumentId() {
		return documentId;
	}

	public void setDocumentId(String documentId) {
		this.documentId = documentId;
	}

	public String getEvent() {
		return event;
	}

	public void setEvent(String event) {
		this.event = event;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getTransferCount() {
		return transferCount;
	}

	public void setTransferCount(int transferCount) {
		this.transferCount = transferCount;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getKantaResponseCode() {
		return kantaResponseCode;
	}

	public void setKantaResponseCode(String kantaResponseCode) {
		this.kantaResponseCode = kantaResponseCode;
	}

	public String getSrcOrgUnit() {
		return srcOrgUnit;
	}

	public void setSrcOrgUnit(String srcOrgUnit) {
		this.srcOrgUnit = srcOrgUnit;
	}

	public String getSrcServiceProviderId() {
		return srcServiceProviderId;
	}

	public void setSrcServiceProviderId(String srcServiceProviderId) {
		this.srcServiceProviderId = srcServiceProviderId;
	}

	public String getSrcServiceProviderName() {
		return srcServiceProviderName;
	}

	public void setSrcServiceProviderName(String srcServiceProviderName) {
		this.srcServiceProviderName = srcServiceProviderName;
	}

	public String getSrcRegistryKeeperId() {
		return srcRegistryKeeperId;
	}

	public void setSrcRegistryKeeperId(String srcRegistryKeeperId) {
		this.srcRegistryKeeperId = srcRegistryKeeperId;
	}

	public String getSrcRegistryKeeperName() {
		return srcRegistryKeeperName;
	}

	public void setSrcRegistryKeeperName(String srcRegistryKeeperName) {
		this.srcRegistryKeeperName = srcRegistryKeeperName;
	}

	public Integer getSrcRegistry() {
		return srcRegistry;
	}

	public void setSrcRegistry(Integer srcRegistry) {
		this.srcRegistry = srcRegistry;
	}

	public String getSrcRegistrySpecifier() {
		return srcRegistrySpecifier;
	}

	public void setSrcRegistrySpecifier(String srcRegistrySpecifier) {
		this.srcRegistrySpecifier = srcRegistrySpecifier;
	}

	public String getSrcRegistrySpecifierName() {
		return srcRegistrySpecifierName;
	}

	public void setSrcRegistrySpecifierName(String srcRegistrySpecifierName) {
		this.srcRegistrySpecifierName = srcRegistrySpecifierName;
	}

	public String getDestOrgUnit() {
		return destOrgUnit;
	}

	public void setDestOrgUnit(String organizationCode) {
		this.destOrgUnit = organizationCode;
	}

	public String getLilmDocId() {
		return lilmDocId;
	}

	public void setLilmDocId(String lilmDocId) {
		this.lilmDocId = lilmDocId;
	}

	public String getDestServiceProviderId() {
		return destServiceProviderId;
	}

	public void setDestServiceProviderId(String destServiceProviderId) {
		this.destServiceProviderId = destServiceProviderId;
	}

	public String getDestServiceProviderName() {
		return destServiceProviderName;
	}

	public void setDestServiceProviderName(String destServiceProviderName) {
		this.destServiceProviderName = destServiceProviderName;
	}

	public String getDestRegistryKeeperId() {
		return destRegistryKeeperId;
	}

	public void setDestRegistryKeeperId(String registryKeeperId) {
		this.destRegistryKeeperId = registryKeeperId;
	}

	public String getDestRegistryKeeperName() {
		return destRegistryKeeperName;
	}

	public void setDestRegistryKeeperName(String registryKeeperName) {
		this.destRegistryKeeperName = registryKeeperName;
	}

	public Integer getDestRegistry() {
		return destRegistry;
	}

	public void setDestRegistry(Integer registry) {
		this.destRegistry = registry;
	}

	public String getDestRegistrySpecifier() {
		return destRegistrySpecifier;
	}

	public void setDestRegistrySpecifier(String registrySpecifier) {
		this.destRegistrySpecifier = registrySpecifier;
	}

	public String getDestRegistrySpecifierName() {
		return destRegistrySpecifierName;
	}

	public void setDestRegistrySpecifierName(String destRegistrySpecifierName) {
		this.destRegistrySpecifierName = destRegistrySpecifierName;
	}

	public String getAssignedCustodianId() {
		return assignedCustodianId;
	}

	public void setAssignedCustodianId(String assignedCustodianId) {
		this.assignedCustodianId = assignedCustodianId;
	}

	public String getAssignedCustodianName() {
		return assignedCustodianName;
	}

	public void setAssignedCustodianName(String assignedCustodianName) {
		this.assignedCustodianName = assignedCustodianName;
	}

	public String getRepresentedOrganizationId() {
		return representedOrganizationId;
	}

	public void setRepresentedOrganizationId(String representedOrganizationId) {
		this.representedOrganizationId = representedOrganizationId;
	}

	public String getRepresentedOrganizationName() {
		return representedOrganizationName;
	}

	public void setRepresentedOrganizationName(String representedOrganizationName) {
		this.representedOrganizationName = representedOrganizationName;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public LocalDateTime getModified() {
		return modified;
	}

	public void setModified(LocalDateTime modified) {
		this.modified = modified;
	}

	@Override
	public String toString() {
		return "LilmData{" + "id=" + id + ", patientId='" + patientId + '\'' + ", patientName='" + patientName + '\''
				+ ", userId='" + userId + '\'' + ", stuId='" + stuId + '\'' + ", documentId='" + documentId + '\''
				+ ", event='" + event + '\'' + ", application='" + application + '\'' + ", time='" + time + '\''
				+ ", status=" + status + '}';
	}
}
