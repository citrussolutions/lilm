package fi.siemens.lilm.processor;

import fi.siemens.lilm.utils.LilmConstants;
import fi.siemens.lilm.utils.LogUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.freemarker.FreemarkerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class SoapProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(SoapProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug(LogUtils.BEGIN);

		var mrMessage = exchange.getIn().getBody(String.class);

		var headers = (Map) exchange.getIn().getHeader(FreemarkerConstants.FREEMARKER_DATA_MODEL);
		headers.put(LilmConstants.MR_MESSAGE, mrMessage);

		logger.debug(LogUtils.END);
	}
}
