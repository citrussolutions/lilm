package fi.siemens.lilm.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fi.siemens.lilm.utils.LogUtils;

public class KantaRequestProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(KantaRequestProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug(LogUtils.BEGIN);

		exchange.getIn().removeHeaders("*");
		exchange.getIn().setHeader(CxfConstants.OPERATION_NAME,
				"RCMR_AR000003_RCMR_IN100002FI01_Asiakirjan_Arkistointi");
		exchange.getIn().setHeader(CxfConstants.OPERATION_NAMESPACE, "urn:hl7-org:v3");

		logger.debug(LogUtils.END);
	}
}
