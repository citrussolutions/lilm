package fi.siemens.lilm.processor;

import fi.siemens.lilm.entity.LilmData;
import fi.siemens.lilm.utils.BirthTime;
import fi.siemens.lilm.utils.DateUtils;
import fi.siemens.lilm.utils.LilmConstants;
import fi.siemens.lilm.utils.LogUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.freemarker.FreemarkerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class LilmProcessor implements Processor {

    @Value("${kanta.receiver.oid}")
    private String kantaReceiverId;

    private static Logger logger = LoggerFactory.getLogger(LilmProcessor.class);

    @Override
    public void process(Exchange exchange) {

        logger.debug(LogUtils.BEGIN);

        var map = (Map<String, List<LilmData>>) exchange.getProperty(LilmConstants.LILM_DATA);

        var key = map.keySet().stream().findFirst().get();

        var lilmDataList = map.remove(key);
        var lilm = lilmDataList.get(0);

        try {
            Map<String, Object> data = new HashMap<>();

            data.put("receiverId", kantaReceiverId);

            data.put("lilmDocId", lilm.getLilmDocId());
            data.put("givenName", lilm.getGivenName());
            data.put("familyName", lilm.getFamilyName());
            data.put("patientid", lilm.getPatientId());
            data.put("birthtime", lilm.getBirthDate());
            data.put("birthdate", BirthTime.resolve(lilm.getBirthDate()));
            data.put("documentIds", lilmDataList.stream().map(l -> l.getStuId()).collect(Collectors.toList()));

            data.put("srcRegistry", lilm.getSrcRegistry());
            data.put("srcRegistrySpecifier", lilm.getSrcRegistrySpecifier());
            data.put("srcRegistrySpecifierName", lilm.getSrcRegistrySpecifierName());
            data.put("srcRegistryDisplayName", getRegistryDisplayName(lilm.getSrcRegistry()));
            data.put("srcRegistryKeeperId", lilm.getSrcRegistryKeeperId());
            data.put("srcRegistryKeeperName", lilm.getSrcRegistryKeeperName());
            data.put("srcServiceProviderId", lilm.getSrcServiceProviderId());
            data.put("srcServiceProviderName", lilm.getSrcServiceProviderName());

            data.put("destRegistry", lilm.getDestRegistry());
            data.put("destRegistrySpecifier", lilm.getDestRegistrySpecifier());
            data.put("destRegistrySpecifierName", lilm.getDestRegistrySpecifierName());
            data.put("destRegistryDisplayName", getRegistryDisplayName(lilm.getDestRegistry()));
            data.put("destRegistryKeeperId", lilm.getDestRegistryKeeperId());
            data.put("destRegistryKeeperName", lilm.getDestRegistryKeeperName());
            data.put("destServiceProviderId", lilm.getDestServiceProviderId());
            data.put("destServiceProviderName", lilm.getDestServiceProviderName());

            data.put("assignedCustodianId", lilm.getAssignedCustodianId());
            data.put("assignedCustodianName", lilm.getAssignedCustodianName());
            data.put("representedOrganizationId", lilm.getRepresentedOrganizationId());
            data.put("representedOrganizationName", lilm.getRepresentedOrganizationName());

            // dates
            data.put("timeAsDisplayDateTime", DateUtils.getLocalDateTimeAsDisplayDateTime(lilm.getTime()));
            data.put("timeAsDateTime", DateUtils.getLocalDateTimeAsDateTime(lilm.getTime()));
            data.put("dateNowAsDisplayDate", DateUtils.getCurrentDate(DateUtils.FORMAT_DATE_DISPLAY));
            data.put("dateNowAsDate", DateUtils.getCurrentDate(DateUtils.FORMAT_DATE));
            data.put("dateNowAsDateTime", DateUtils.getCurrentDateTime());

            var signatureTimestamp = DateUtils.getCurrentDateTimeAsISO();

            data.put("signatureTimestamp", signatureTimestamp);
            data.put("signatureTimestampAsId", DateUtils.getSignatureTimestampAsId(signatureTimestamp));

            exchange.getIn().setHeader(FreemarkerConstants.FREEMARKER_DATA_MODEL, data);
            exchange.setProperty(LilmConstants.LILM_DATA, map.isEmpty() ? null : map);

        } finally {
            exchange.setProperty(LilmConstants.PROCESSED_LILM_DATA, lilmDataList);
        }
        logger.debug(LogUtils.END);
    }

    private static String getRegistryDisplayName(Integer registry) {
        return registry == LilmConstants.REGISTRY_TTH ? LilmConstants.REGISTRY_DISPLAY_NAME_TTH
                : LilmConstants.REGISTRY_DISPLAY_NAME_TTH;
    }
}
