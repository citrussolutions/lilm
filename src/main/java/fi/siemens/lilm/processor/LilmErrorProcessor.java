package fi.siemens.lilm.processor;

import fi.siemens.lilm.entity.LilmData;
import fi.siemens.lilm.utils.LilmConstants;
import fi.siemens.lilm.utils.LogUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.List;

public class LilmErrorProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(LilmErrorProcessor.class);

	@Override
	public void process(Exchange exchange) throws IOException {

		logger.debug(LogUtils.BEGIN);

		// the caused by exception is stored in a property on the exchange
		Throwable caused = exchange.getProperty(Exchange.EXCEPTION_CAUGHT, Throwable.class);

		StringBuilder errorMessage = new StringBuilder();

		if (caused != null) {
			try (var sw = new StringWriter(); var pw = new PrintWriter(sw);) {
				caused.printStackTrace(pw);
				errorMessage.append(sw);
			}
		}

		if (errorMessage.isEmpty()) {
			errorMessage.append("Unknown error occured");
		}

		var status = LilmData.Status.ERROR;

		var lilmDataList = (List<LilmData>) exchange.getProperty(LilmConstants.PROCESSED_LILM_DATA);

		if (lilmDataList != null) {
			for (var lilmData : lilmDataList) {
				lilmData.setStatus(status);
				lilmData.setMessage(errorMessage.toString());
				lilmData.setModified(LocalDateTime.now());

				if (status == LilmData.Status.SEND_ERROR) {
					lilmData.setTransferCount(lilmData.getTransferCount() + 1);
				}
			}
		}

		exchange.getIn().setBody(lilmDataList);

		logger.debug(LogUtils.END);
	}
}
