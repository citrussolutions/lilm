package fi.siemens.lilm.processor;

import java.io.FileInputStream;
import java.io.InputStream;
import java.security.KeyStore;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.TrustManagerFactory;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.cxf.configuration.jsse.TLSClientParameters;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.hl7.v3.RCMRAR000003ArkistoService;
import org.hl7.v3.RCMRIN100002FI01;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;

import fi.siemens.lilm.utils.LogUtils;

public class KantaProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(KantaProcessor.class);

	@Value("${uri.kanta.endpoint}")
	private String kantaEndpointAddress;

	@Value("${timeout.kanta.endpoint}")
	private long timeoutKantaEndpoint;

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug(LogUtils.BEGIN);

		JaxWsProxyFactoryBean factory = createJaxWsProxyFactory(kantaEndpointAddress);
		RCMRAR000003ArkistoService kantaClient = (RCMRAR000003ArkistoService) factory.create();

		try (Client proxy = ClientProxy.getClient(kantaClient)) {

			HTTPConduit conduit = (HTTPConduit) proxy.getConduit();
			conduit.setClient(getHttpClientPolicy());

			setupTLS(conduit);

			RCMRIN100002FI01 body = (RCMRIN100002FI01) exchange.getIn().getBody();
			kantaClient.getRCMRAR000003ArkistoPort().rcmrAR000003RCMRIN100002FI01AsiakirjanArkistointi(body);
		}

		logger.debug(LogUtils.END);
	}

	private JaxWsProxyFactoryBean createJaxWsProxyFactory(String address) {

		logger.debug(LogUtils.BEGIN);

		JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
		factory.setServiceClass(RCMRAR000003ArkistoService.class);
		factory.setAddress(address);

		logger.debug(LogUtils.END);

		return factory;
	}

	private HTTPClientPolicy getHttpClientPolicy() {

		logger.debug(LogUtils.BEGIN);

		HTTPClientPolicy clientPolicy = new HTTPClientPolicy();
		clientPolicy.setConnectionTimeout(timeoutKantaEndpoint);
		clientPolicy.setReceiveTimeout(timeoutKantaEndpoint);

		logger.debug(LogUtils.END);

		return clientPolicy;
	}

	private void setupTLS(HTTPConduit conduit) throws Exception {

		logger.debug(LogUtils.BEGIN);

		TLSClientParameters tlsCP = new TLSClientParameters();
		tlsCP.setDisableCNCheck(true);

		KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
		try (InputStream is = new FileInputStream("src/main/config/clientKeystore.jks")) {
			keyStore.load(is, "cspass".toCharArray());
		}

		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(keyStore, "ckpass".toCharArray());
		tlsCP.setKeyManagers(kmf.getKeyManagers());

		TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
		tmf.init(keyStore);
		tlsCP.setTrustManagers(tmf.getTrustManagers());

		conduit.setTlsClientParameters(tlsCP);

		logger.debug(LogUtils.END);
	}
}
