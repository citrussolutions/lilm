package fi.siemens.lilm.processor;

import fi.siemens.lilm.entity.LilmData;
import fi.siemens.lilm.repository.RegistryRepository;
import fi.siemens.lilm.utils.DateUtils;
import fi.siemens.lilm.utils.LogUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.file.GenericFile;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.ArrayList;

@Component
public class LogFileProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(LogFileProcessor.class);

	private RegistryRepository registryRepository;

	public LogFileProcessor(RegistryRepository registryRepository) {
		this.registryRepository = registryRepository;
	}

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug(LogUtils.BEGIN);

		var lilmDataList = new ArrayList<LilmData>();

		// log file parsing
		//
		var logFile = (GenericFile)exchange.getIn().getBody();

		var logLines = Files.readAllLines(Paths.get(logFile.getAbsoluteFilePath()));

		for (var line : logLines) {

			var lilmData = new LilmData();

			String fileContent = line.replaceAll("\"", "");

			var tokens = fileContent.split("\\u007C");

			for (var token : tokens) {
				var key = token.substring(0, token.indexOf(":"));
				var value = token.replace(key + ":", "");

				setLilmDataValue(lilmData, key, value);

				// add extra values to match freemarker template
				//
				if (key.equals("patname")) {
					var familyname = value.substring(value.lastIndexOf(" ")).trim();
					var givenname = value.replace(familyname, "").trim();

					lilmData.setGivenName(givenname);
					lilmData.setFamilyName(familyname);
				}
			}

			lilmData.setCreated(LocalDateTime.now());
			lilmData.setStatus(LilmData.Status.SAVED);

			lilmDataList.add(lilmData);
		}

		exchange.getIn().setBody(lilmDataList);

		logger.debug(LogUtils.END);
	}

	private void setLilmDataValue(LilmData lilmData, String key, String value) {

		logger.debug(LogUtils.BEGIN);

		switch (key.toLowerCase()) {
			case "patid":
				lilmData.setPatientId(value);
				break;
			case "patname":
				lilmData.setPatientName(value);
				break;
			case "birthdate":
				lilmData.setBirthDate(value);
				break;
			case "userid":
				lilmData.setUserId(value);
				break;
			case "stuid":
				lilmData.setStuId(value);
				break;
			case "document_id":
				lilmData.setDocumentId(value);
				break;
			case "event":
				lilmData.setEvent(value);
				break;
			case "application":
				lilmData.setApplication(value);
				break;
			case "time":
				lilmData.setTime(DateUtils.getPacsTimeAsLocalDateTime(value));
				break;

			case "src_orgunit": // source orginization
				var optionalSrcRegistry = registryRepository.findById(value);
				if (optionalSrcRegistry.isPresent()) {

					lilmData.setSrcOrgUnit(value);

					var registry = optionalSrcRegistry.get();

					lilmData.setSrcServiceProviderId(registry.getServiceProviderId());
					lilmData.setSrcServiceProviderName(registry.getServiceProviderName());
					lilmData.setSrcRegistryKeeperId(registry.getRegistryKeeperId());
					lilmData.setSrcRegistryKeeperName(registry.getRegistryKeeperName());
					lilmData.setSrcRegistry(registry.getRegistry());
					lilmData.setSrcRegistrySpecifier(registry.getRegistrySpecifier());
					lilmData.setSrcRegistrySpecifierName(registry.getRegistrySpecifierName());

				}
				break;

			case "dst_orgunit": // Receiving organization
				var optionalDestRegistry = registryRepository.findById(value);
				if (optionalDestRegistry.isPresent()) {

					lilmData.setDestOrgUnit(value);

					var registry = optionalDestRegistry.get();
					lilmData.setDestServiceProviderId(registry.getServiceProviderId());
					lilmData.setDestServiceProviderName(registry.getServiceProviderName());
					lilmData.setDestRegistryKeeperId(registry.getRegistryKeeperId());
					lilmData.setDestRegistryKeeperName(registry.getRegistryKeeperName());
					lilmData.setDestRegistry(registry.getRegistry());
					lilmData.setDestRegistrySpecifier(registry.getRegistrySpecifier());
					lilmData.setDestRegistrySpecifierName(registry.getRegistrySpecifierName());

					// automatic sharing stores document to destination registry
					lilmData.setAssignedCustodianId(registry.getAssignedCustodianId());
					lilmData.setAssignedCustodianName(registry.getAssignedCustodianName());
					lilmData.setRepresentedOrganizationId(registry.getRepresentedOrganizationId());
					lilmData.setRepresentedOrganizationName(registry.getRepresentedOrganizationName());

				}
				break;

			default: break;
		}

		logger.debug(LogUtils.END);
	}
}
