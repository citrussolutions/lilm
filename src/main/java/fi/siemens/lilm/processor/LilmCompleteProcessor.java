package fi.siemens.lilm.processor;

import fi.siemens.lilm.entity.LilmData;
import fi.siemens.lilm.utils.LilmConstants;
import fi.siemens.lilm.utils.LogUtils;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.language.xpath.XPathBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.LocalDateTime;
import java.util.List;

public class LilmCompleteProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(LilmCompleteProcessor.class);
	private static final String XPATH_ACK_CODE = "/v3:RCMR_IN100002FI01_ArkistoResponse/v3:RCMR_IN120001FI01/v3:acknowledgement/v3:typeCode/@code";
	private static final String OK_ACK_CODE = "AA";

	@Override
	public void process(Exchange exchange) throws Exception {

		String responseCode = getKantaResponseCode(exchange);

		var lilmDataList = (List<LilmData>) exchange.getProperty(LilmConstants.PROCESSED_LILM_DATA);

		for (var lilmData : lilmDataList) {
			lilmData.setStatus((OK_ACK_CODE.equals(responseCode)) ? LilmData.Status.COMPLETE : LilmData.Status.SEND_ERROR);
			lilmData.setKantaResponseCode(responseCode);
			lilmData.setTransferCount(lilmData.getTransferCount() + 1);
			lilmData.setModified(LocalDateTime.now());
		}
		exchange.getIn().setBody(lilmDataList);

		logger.debug(LogUtils.END);
	}

	/**
	 * Returns kanta-endpoint responsecode
	 * 
	 * @param exchange the message exchange
	 * @return kanta-endpoint responsecode
	 */
	private String getKantaResponseCode(Exchange exchange) {

		logger.debug(LogUtils.BEGIN);

		//@formatter:off
		String responseCode = XPathBuilder.xpath(XPATH_ACK_CODE)
				.namespace("v3", "urn:hl7-org:v3")
				.namespace("hl7", "urn:hl7finland")				
				.stringResult().evaluate(exchange, String.class);
		//@formatter:on

		logger.debug(LogUtils.END_WITH_PARAM, responseCode);

		return responseCode;
	}
}