package fi.siemens.lilm.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.siemens.lilm.signature.XmlSigner;
import fi.siemens.lilm.utils.LogUtils;

@Component
public class SignatureProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(SignatureProcessor.class);

	private XmlSigner xmlSigner;

	@Autowired
	public SignatureProcessor(XmlSigner xmlSigner) {
		this.xmlSigner = xmlSigner;
	}

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug(LogUtils.BEGIN);

		var cda = (String) exchange.getIn().getBody();

		String signedCda = xmlSigner.sign(cda);

		exchange.getIn().setBody(signedCda);

		logger.debug(LogUtils.END);
	}
}
