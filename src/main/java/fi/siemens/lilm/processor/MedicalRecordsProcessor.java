package fi.siemens.lilm.processor;

import java.util.Map;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.apache.camel.component.freemarker.FreemarkerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fi.siemens.lilm.utils.LilmConstants;
import fi.siemens.lilm.utils.LogUtils;

public class MedicalRecordsProcessor implements Processor {

	private static Logger logger = LoggerFactory.getLogger(MedicalRecordsProcessor.class);

	@Override
	public void process(Exchange exchange) throws Exception {

		logger.debug(LogUtils.BEGIN);

		var base64EncodedCda = exchange.getIn().getBody(String.class);
		base64EncodedCda = base64EncodedCda.substring(base64EncodedCda.indexOf("MIME-Version"));

		var headers = (Map) exchange.getIn().getHeader(FreemarkerConstants.FREEMARKER_DATA_MODEL);
		headers.put(LilmConstants.CDA_BASE64, base64EncodedCda);

		logger.debug(LogUtils.END);
	}
}
