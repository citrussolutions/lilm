package fi.siemens.lilm.utils;

public class LilmConstants {
    public static String LILM_DATA = "lilm_data";
    public static String PROCESSED_LILM_DATA = "processed_lilm_data";
    public static String CDA_BASE64 = "cda_base64";

    public static String MR_MESSAGE = "mr_message";

    public static Integer REGISTRY_PUBLIC = 2;
    public static Integer REGISTRY_TTH = 4;

    public static String REGISTRY_DISPLAY_NAME_PUBLIC = "Julkinen terveydenhuolto";
    public static String REGISTRY_DISPLAY_NAME_TTH = "Työterveyshuolto";

    public static final String CDA_FI_XSD_PATH = "src/main/resources/schemas/cda/schema-xml-muotoilu/CDA_Fi.xsd";

}
