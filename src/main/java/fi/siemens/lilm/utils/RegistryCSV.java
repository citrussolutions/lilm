package fi.siemens.lilm.utils;

import org.apache.logging.log4j.util.Strings;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.stream.Collectors;

public class RegistryCSV {

    public static String generateSQL() throws IOException {

        var logLines = Files.readAllLines(Paths.get("./data/rekisterit.csv"), StandardCharsets.UTF_8);

        var columns = logLines.remove(0).split(";");
        var sb = new StringBuilder();

        for (var line : logLines) {

            var vals = Arrays.stream(line.split(";"))
                    .map(s -> Strings.isBlank(s) ? "NULL" : "'" + s + "'")
                    .collect(Collectors.joining(", "));

            sb.append("INSERT INTO Registry (");
            sb.append(String.join(",",columns));
            sb.append(") VALUES(");
            sb.append(vals);
            sb.append(");\n");
        }

        return sb.toString();
    }
}
