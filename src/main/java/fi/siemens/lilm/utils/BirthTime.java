package fi.siemens.lilm.utils;

public class BirthTime {

    public static String resolve(String birthdate) {

        var tokens = birthdate.split("\\.");

        var year = tokens[2];
        var month = tokens[1];
        var date = tokens[0];

        return year + month + date;
    }

    /*public static String resolve(String hetu) {

        if (hetu == null || hetu.length() != 11) return "";

        String year = "";

        switch (hetu.charAt(6)) {
            case 'A':
                year = "20";
                break;
            case '-':
                year = "19";
                break;
            case '+':
                year = "18";
                break;

            default: break;
        }

        year += hetu.substring(4, 6);

        var month = hetu.substring(2, 4);
        var day = hetu.substring(0, 2);

        return year + month + day;
    }*/
}
