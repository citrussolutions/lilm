package fi.siemens.lilm.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class OidGenerator {

    private String oidSeed;

    public OidGenerator(@Value("${oid-generator.seed}") String seed) {
        if (!seed.endsWith(".")) {
            seed += ".";
        }
        oidSeed = seed;
    }

    public String create() {
        LocalDate now = LocalDate.now();

        return String.format("%s%d.%d.%d.%d",
                oidSeed,
                now.getYear(),
                now.getMonthValue(),
                now.getDayOfMonth(),
                System.currentTimeMillis());
    }
}
