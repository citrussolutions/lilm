package fi.siemens.lilm.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    public static String FORMAT_DATE = "yyyyMMdd";
    public static String FORMAT_DATE_DISPLAY = "dd.MM.yyyy";
    public static String FORMAT_DATETIME_DISPLAY = "dd.MM.yyyy HH:mm:ss";
    private static String FORMAT_DATETIME = "yyyyMMddHHmmss";
    private static String FORMAT_PACS_TIME = "yyyy-MM-dd HH:mm:ss";
    private static String FORMAT_ISO_TIME = "yyyy-MM-dd'T'HH:mm:ssXXX";
    private static String FORMAT_PACS_ISO_TIME = "yyyy-MM-dd'T'HH:mm:ss";
    private static String FORMAT_SIGNATURE_TIME_DISPLAY = "yyyy.MM.dd.HH.mm.ss";


    public static String getCurrentDate(String format) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(format);
        LocalDate now = LocalDate.now();

        return dtf.format(now);
    }

    public static String getCurrentDateTime() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FORMAT_DATETIME);
        LocalDateTime now = LocalDateTime.now();

        return dtf.format(now);
    }

    public static String getLocalDateTimeAsDisplayDateTime(LocalDateTime time) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FORMAT_DATETIME_DISPLAY);
        return dtf.format(time);
    }

    public static String getLocalDateTimeAsDateTime(LocalDateTime time) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FORMAT_DATETIME);
        return dtf.format(time);
    }

    public static LocalDateTime getPacsTimeAsLocalDateTime(String pacsTime) {
        DateTimeFormatter pacsTimeFormat = DateTimeFormatter.ofPattern(FORMAT_PACS_ISO_TIME);
        return LocalDateTime.parse(pacsTime, pacsTimeFormat);
    }

    public static String getCurrentDateTimeAsISO() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FORMAT_ISO_TIME);
        return ZonedDateTime.now().format(dtf);
    }

    public static String getSignatureTimestampAsId(String signatureTime) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(FORMAT_SIGNATURE_TIME_DISPLAY);
        DateTimeFormatter signatureTimeFormat = DateTimeFormatter.ofPattern(FORMAT_ISO_TIME);

        ZonedDateTime time = ZonedDateTime.parse(signatureTime, signatureTimeFormat);

        return dtf.format(time);
    }
}
