package fi.siemens.lilm.service;

import fi.siemens.lilm.entity.LilmData;
import fi.siemens.lilm.repository.LilmRepository;
import fi.siemens.lilm.utils.LogUtils;
import fi.siemens.lilm.utils.OidGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class LilmService {

	private static Logger logger = LoggerFactory.getLogger(LilmService.class);

	private final int TRANSFER_RETRY_COUNT_MAX = 5;
	private LilmRepository repository;
	private OidGenerator oidGenerator;

	public LilmService(LilmRepository repository, OidGenerator oidGenerator) {
		this.repository = repository;
		this.oidGenerator = oidGenerator;
	}

	/**
	 * Fetch lilm data to be transferred using statuses SAVED and ERROR
	 *
	 * @return List of Lilm data
	 */
	public Map<String, List<LilmData>> fetchData() {

		logger.debug(LogUtils.BEGIN);

		var data = repository.findAllByStatusInAndTransferCountLessThan(
				Arrays.asList(LilmData.Status.SAVED, LilmData.Status.SEND_ERROR), TRANSFER_RETRY_COUNT_MAX);

		var map = data.stream().collect(Collectors.groupingBy(
				l -> l.getPatientId() + "_" + l.getSrcServiceProviderId() + "_" + l.getDestServiceProviderId()));

		for (var entry : map.entrySet()) {
			var list = entry.getValue();
			var documentOid = oidGenerator.create();
			for (var lilmdata : list) {
				lilmdata.setLilmDocId(documentOid);
				lilmdata.setStatus(LilmData.Status.INPROGRESS);
				lilmdata.setModified(LocalDateTime.now());
				repository.saveAndFlush(lilmdata);
			}
		}

		logger.debug(LogUtils.END_WITH_PARAM, map.size());

		return map.isEmpty() ? null : map;
	}
}
