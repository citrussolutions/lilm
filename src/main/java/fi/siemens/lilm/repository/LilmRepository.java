package fi.siemens.lilm.repository;

import fi.siemens.lilm.entity.LilmData;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LilmRepository extends JpaRepository<LilmData, Long> {

    List<LilmData> findAllByStatusInAndTransferCountLessThan(List<LilmData.Status> status, int transferCount);
}
