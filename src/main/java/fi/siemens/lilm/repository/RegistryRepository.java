package fi.siemens.lilm.repository;

import fi.siemens.lilm.entity.Registry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RegistryRepository extends JpaRepository<Registry, String> {
}
