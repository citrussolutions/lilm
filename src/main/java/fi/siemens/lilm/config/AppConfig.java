package fi.siemens.lilm.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
//@formatter:off
@PropertySources(value = { 
		@PropertySource("classpath:application.properties"),
		@PropertySource(value = "file:config/application.properties",
		ignoreResourceNotFound = true) })
//@formatter:on
public class AppConfig {

	/*
	 * @Value("${timeout.kanta.endpoint}") private long timeoutKantaEndpoint;
	 * 
	 * private static final String CLIENT_CERT_KEYSTORE_PASSWORD = ""; private
	 * static final String CLIENT_CERT_KEYSTORE_PATH = ""; private static final
	 * String CLIENT_CERT_TRUSTSTORE_PATH = "";
	 * 
	 * @Bean(name = "kantaEndpoint") public CxfSpringEndpoint
	 * kantaEndpoint(CamelContext camelContext) {
	 * 
	 * CxfComponent cxfComponent = new CxfComponent(camelContext); CxfSpringEndpoint
	 * serviceEndpoint = new CxfSpringEndpoint(cxfComponent,
	 * "{{uri.kanta.endpoint}}");
	 * 
	 * serviceEndpoint.setWsdlURL("classpath:wsdl/RCMR_AR000003_Arkisto.wsdl");
	 * serviceEndpoint.setDataFormat(DataFormat.PAYLOAD);
	 * serviceEndpoint.setServiceNameAsQName(new QName("urn:hl7-org:v3",
	 * "RCMR_AR000003_Arkisto_Service"));
	 * serviceEndpoint.setContinuationTimeout(timeoutKantaEndpoint);
	 * 
	 * SSLContextParameters producerSslContextParameters =
	 * createProducerSSLContextParameters();
	 * serviceEndpoint.setSslContextParameters(producerSslContextParameters);
	 * 
	 * // Not for use in production HostnameVerifier hostnameVerifier = new
	 * HostnameVerifier() {
	 * 
	 * @Override public boolean verify(String hostname, SSLSession session) { return
	 * true; } }; serviceEndpoint.setHostnameVerifier(hostnameVerifier);
	 * 
	 * return serviceEndpoint; }
	 * 
	 * private SSLContextParameters createProducerSSLContextParameters() { final
	 * KeyStoreParameters ksp = new KeyStoreParameters();
	 * ksp.setResource(CLIENT_CERT_KEYSTORE_PATH);
	 * ksp.setPassword(CLIENT_CERT_KEYSTORE_PASSWORD);
	 * 
	 * final KeyManagersParameters kmp = new KeyManagersParameters();
	 * kmp.setKeyStore(ksp); kmp.setKeyPassword(CLIENT_CERT_KEYSTORE_PASSWORD);
	 * 
	 * final SSLContextClientParameters sslContextClientParameters = new
	 * SSLContextClientParameters(); final SSLContextParameters sslContextParameters
	 * = new SSLContextParameters();
	 * sslContextParameters.setClientParameters(sslContextClientParameters);
	 * sslContextParameters.setKeyManagers(kmp);
	 * sslContextParameters.setCertAlias("client");
	 * sslContextParameters.setSecureSocketProtocol("TLSv1.2");
	 * 
	 * // so that the client trusts the self-signed server certificate final
	 * KeyStoreParameters trustStoreParams = new KeyStoreParameters();
	 * trustStoreParams.setResource(CLIENT_CERT_TRUSTSTORE_PATH);
	 * trustStoreParams.setPassword(CLIENT_CERT_KEYSTORE_PASSWORD); final
	 * TrustManagersParameters tmp = new TrustManagersParameters();
	 * tmp.setKeyStore(trustStoreParams);
	 * sslContextParameters.setTrustManagers(tmp);
	 * 
	 * return sslContextParameters; }
	 * 
	 */
}
