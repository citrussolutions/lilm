package fi.siemens.lilm.router;

import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fi.siemens.lilm.processor.KantaProcessor;
import fi.siemens.lilm.processor.KantaRequestProcessor;
import fi.siemens.lilm.processor.LilmCompleteProcessor;
import fi.siemens.lilm.processor.LilmErrorProcessor;
import fi.siemens.lilm.processor.LilmProcessor;
import fi.siemens.lilm.processor.MedicalRecordsProcessor;
import fi.siemens.lilm.processor.SignatureProcessor;
import fi.siemens.lilm.utils.LilmConstants;

/**
 * Camel route for database operations.
 *
 */
@Component
public class LilmDataRouter extends RouteBuilder {

	private LilmProcessor lilmProcessor;
	private SignatureProcessor signatureProcessor;

	@Autowired
	public LilmDataRouter(LilmProcessor lilmProcessor, SignatureProcessor signatureProcessor) {
		this.lilmProcessor = lilmProcessor;
		this.signatureProcessor = signatureProcessor;
	}

	@Override
	public void configure() throws Exception {

		// @formatter:off
        from("timer://fetchDataTimer?period={{data.transfer.frequency}}")
            .routeId("LilmDataRouter")
            .log("step1: read data from database")
            .bean("lilmService", "fetchData").setProperty(LilmConstants.LILM_DATA, body()) 
            .choice()
                .when(body().isNull()).log("Fetch data end. Nothing to transfer...").endChoice()
                .otherwise()
                .loopDoWhile(exchangeProperty(LilmConstants.LILM_DATA).isNotNull())

                    .doTry()
                        .log("step2: create cda r2 file with freemarker")
                        .process(lilmProcessor).id("LilmDataRouter.lilmProcessor")
                        .to("freemarker:templates/lilm/cdar2_automatic.ftl")
                        
                        .log("step3: validate cda r2 file against CDA_Fi.xsd schema")
                        .to("validator:schemas/cda/schema-xml-muotoilu/CDA_Fi.xsd")

                        .log("step4: call xml-signing enpoint")
                        .process(signatureProcessor).id("LilmDataRouter.signatureProcessor")
                        
                        .log("step5: validate cda r2 file against CDA_Fi.xsd schema after signing")
                        .to("validator:schemas/cda/schema-xml-muotoilu/CDA_Fi.xsd")
                        
                        .log("step6: create Mime-message")                       
                        .marshal().mimeMultipart("related", true, true, false)
                                                          
                        .log("step7: create mr-message with freemarker")
                        .process(new MedicalRecordsProcessor()).id("LilmDataRouter.medicalRecordsProcessor")
                        .to("freemarker:templates/lilm/mr_message.ftl")

                        .log("step8: validate cda medical records message against RCMR_IN100002FI01.xsd schema")
                        .to("validator:schemas/xsd/RCMR_IN100002FI01.xsd")
			
						.log("step9: send mr-message to output folder")
                        .to("file:{{logfile.output.folder}}?fileName=lilm_${date:now:yyyyMMddHHmmss}.xml")
                        .delay(1000) // to get unique file names
                                           
                        .log("step10: build kanta-requestmessage")
                        .process(new KantaRequestProcessor()).id("LilmDataRouter.kantaRequestProcessor")
                                                
                        .log("step11: send kanta-requestmessage to kanta-endpoint")
                        .process(new KantaProcessor()).id("LilmDataRouter.kantaProcessor")
                        //.to("cxf:bean:kantaEndpoint")
                        
                        .log("step12: handle kanta responsemessage")
                        .process(new LilmCompleteProcessor()).id("LilmDataRouter.lilmCompleteProcessor")

                        .log("step13: update lilm complete status")
                        .to("jpa:fi.siemens.entity.LilmData")

                        .end()
                    .endDoTry()
                    .doCatch(Exception.class)                       
                        .process(new LilmErrorProcessor()).id("LilmDataRouter.lilmErrorProcessor")
                        .log("step error: update lilm error status")
                        .to("jpa:fi.siemens.entity.LilmData")
                        .end()
                    .end()
                .end() // end of loop
            .end(); // end of choice
    }
}
