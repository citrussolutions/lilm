package fi.siemens.lilm.router;

import fi.siemens.lilm.processor.LogFileProcessor;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class LilmLogFileRouter extends RouteBuilder {

	private LogFileProcessor logFileProcessor;

	public LilmLogFileRouter(LogFileProcessor logFileProcessor) {
		this.logFileProcessor = logFileProcessor;
	}

	@Override
	public void configure() {

		// @formatter:off
    	from("file:{{logfile.input.folder}}?delete=true&delay={{polling.delay}}&maxMessagesPerPoll=1&moveFailed={{logfile.error.folder}}")
    		.routeId("LilmLogFileRouter")

			.log("step1: read .csv-file")
			.log("step2: bind pacs log file data to pojo")
			.process(logFileProcessor).id("LilmLogFileRouter.logFileProcessor")
			.log("step3: insert lilm log data to database")
			.to("jpa:fi.citrus.entity.LilmData")
			.end();
    	// @formatter:on
	}

}
