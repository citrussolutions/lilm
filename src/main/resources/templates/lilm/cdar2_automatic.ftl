<#ftl output_format="XML" encoding="utf-8">
<?xml version="1.0" encoding="UTF-8"?>
<ClinicalDocument xmlns="urn:hl7-org:v3"
                  xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:hl7fi="urn:hl7finland"
                  xsi:schemaLocation="urn:hl7-org:v3 ..\..\skeemat\cda\schema-xml-muotoilu\CDA_fi.xsd">
    <!-- UUSI LILM
                - automaattiluovutus
                - 2 kpl luovutuksen antajaa eli luovutuksen antaja toistuus (.99999999. ja .88888888.)
                - luovutuksen antajan alla 2 kpl "näkymä+palvelutapahtuma" luovutus
                - automaattiluovutus, joten arkistoidaan saajan nimissä ja saajan rekisteriin (.77777777.)
     -->
    <!--
    ********************************************************
    HL7 CDA R2 header tiedot
    ********************************************************
    -->
    <!-- 1. Asiakirjan aluekoodi -->
    <realmCode code="FI"/>
    <!-- 2. Asiakirjassa noudatettu tekninen standardiversio CDA R2 Ansi Standard 2005-04-21-->
    <typeId root="2.16.840.1.113883.1.3" extension="POCD_HD000040"/>
    <!-- 3. Asiakirjassa noudatettu määritys -->
    <!-- 3. eArkiston asiakirjojen kuvailutiedot versio 2.40 - -->
    <templateId root="1.2.246.777.11.2015.36"/>
    <!-- 3. CDA R2 Header versio 4.67 -->
    <templateId root="1.2.246.777.11.2015.38"/>
    <!-- 3. CDA R2 Kertomus ja lomakkeet versio 4.68 -->
    <templateId root="1.2.246.777.11.2015.10"/>
    <!-- 4. Asiakirjan yksilöintitunnus -->
    <id root="${headers.CamelFreemarkerDataModel.lilmDocId}"/>
    <!-- 5. Asiakirjan potilasrekisteritunnus -->
    <code code="13" displayName="Arkistoasiakirjat" codeSystem="1.2.246.537.5.40150.2009"
          codeSystemName="KanTa-palvelut - Potilasasiakirjan rekisteritunnus"/>
    <!-- 6. Asiakirjan otsikko -->
    <title>Luovutusilmoitusasiakirja</title>
    <!-- 7. Asiakirjan luontiaika -->
    <effectiveTime value="${headers.CamelFreemarkerDataModel.dateNowAsDateTime}"/>
    <!-- 8. Asiakirjan luottamuksellisuus -->
    <confidentialityCode code="5" displayName="Terveydenhuollon salassapidettävä" codeSystem="1.2.246.777.5.99902.2006"
                         codeSystemName="KanTa-palvelut - Asiakirjan luottamuksellisuus"/>
    <!-- 9. Asiakirjan kieli -->
    <languageCode code="fi"/>
    <!-- 10. Alkuperäisen asiakirjan yksilöintitunnus -->
    <setId root="${headers.CamelFreemarkerDataModel.lilmDocId}"/>
    <!-- 11. Asiakirjan versio -->
    <versionNumber value="1"/>
    <!-- 13. Potilaan perustiedot -->
    <recordTarget>
        <!-- Potilas -->
        <patientRole>
            <!-- Potilaan henkilötunnus -->
            <id extension="${headers.CamelFreemarkerDataModel.patientid}" root="1.2.246.21"/>
            <!-- Potilaan muu tunnus -->
            <patient>
                <!-- Potilaan nimi -->
                <name>
                    <!-- Potilaan kutsumanimi, optionaalinen -->
                    <given qualifier="CL">${headers.CamelFreemarkerDataModel.givenName}</given>
                    <!-- Potilaan etunimi1, pakollinen -->
                    <given>${headers.CamelFreemarkerDataModel.givenName}</given>
                    <!-- Potilaan sukunimi, pakollinen  -->
                    <family>${headers.CamelFreemarkerDataModel.familyName}</family>
                </name>
                <!-- Potilaan syntymäaika -->
                <birthTime value="${headers.CamelFreemarkerDataModel.birthdate}"/>
            </patient>
        </patientRole>
    </recordTarget>
    <!-- 14. Ammattihenkilö, tiedot tyhjäksi kun automaattiluovutus -->
    <author>
        <time nullFlavor="NA"/>
        <assignedAuthor>
            <id nullFlavor="NA"/>
        </assignedAuthor>
    </author>
    <!-- 17. Asiakirjan rekisterinpitäjä, automaattiluovutuksessa luovutuksen saajan rekisterinpitäjä -->
    <custodian>
        <assignedCustodian>
            <representedCustodianOrganization>
                <id root="${headers.CamelFreemarkerDataModel.destRegistryKeeperId}"/>
                <name>${headers.CamelFreemarkerDataModel.destRegistryKeeperName}</name>
            </representedCustodianOrganization>
        </assignedCustodian>
    </custodian>
    <!-- 26. Palvelutapahtuman käyntitiedot, LILMit eivät liity palvelutapahtumaan mutta palveluntuottaja ilmaistava tässä rakenteessa, automaattiluovutuksessa Luovutuksen saajan palveluntuottaja -->
    <componentOf>
        <encompassingEncounter>
            <!-- 26.1 Palvelutapahtumatunnus, LILMit eivät liity palvelutapahtumaan  -->
            <!-- 26.3 Palvelutapahtuman kokonaishoitoaika (käytetään vain palvelutapahtuma-asiakirjoissa, pakollinen skeemassa joten LILMillä NA -->
            <effectiveTime nullFlavor="NA"/>
            <!-- 26.4 Palveluntuottaja -->
            <responsibleParty>
                <assignedEntity>
                    <id nullFlavor="NA"/>
                    <representedOrganization>
                        <!-- palveluntuottaja -->
                        <id root="${headers.CamelFreemarkerDataModel.representedOrganizationId}"/>
                        <name>${headers.CamelFreemarkerDataModel.representedOrganizationName}</name>
                    </representedOrganization>
                </assignedEntity>
            </responsibleParty>
        </encompassingEncounter>
    </componentOf>
    <!--
********************************************************
HL7 CDA R2 Local Header
********************************************************
-->
    <hl7fi:localHeader>
        <!-- FI 2 hl7fi:tableOfContents - asiakirjan sisällysluettelo -->
        <hl7fi:tableOfContents>
            <hl7fi:contentsCode code="322" displayName="Luovutusilmoitusasiakirja" codeSystem="1.2.246.537.6.12.2002"
                                codeSystemName="AR/YDIN - Näkymät"/>
        </hl7fi:tableOfContents>
        <!-- FI 6 hl7fi:fileFormat - asiakirjan tiedostomuoto poistettu -->
        <hl7fi:fileFormat code="1" displayName="CDA R2" codeSystem="1.2.246.537.5.40179.2008"
                          codeSystemName="eArkisto - Asiakirjan tiedostomuoto"/>
        <!-- FI 7 hl7fi:softwareSupport - asiakirjan tuottanut ohjelmisto  -->
        <hl7fi:softwareSupport moderator="Yritys Oy" product="Aluejärjestelmä ZYX" version="1.12">Yritys Oy
            Aluejärjestelmä ZYX 1.12
        </hl7fi:softwareSupport>
        <!-- FI 9 hl7fi:documentType - asiakirjan tyyppi -->
        <hl7fi:documentType code="3" displayName="Arkistoasiakirja" codeSystem="1.2.246.537.5.5001.2011"
                            codeSystemName="eArkisto - Asiakirjatyyppi"/>
        <!-- FI 12 hl7fi:functionCode - asiakirjan tehtäväluokka (eAMS) -->
        <hl7fi:functionCode code="06.00.02" displayName="Potilasasiakirjojen käytön ja luovutusten hallinta"
                            codeSystem="1.2.246.537.6.300.2010"/>
        <!-- FI 13 hl7fi:recordStatus - asiakirjan tila -->
        <hl7fi:recordStatus code="3" displayName="Arkistointivalmis" codeSystem="1.2.246.537.5.40154.2008"
                            codeSystemName="eArkisto - Asiakirjan valmistumisen tila"/>
        <!-- FI 18 hl7fi:signatureCollection - Allekirjoitukset, ks. header doku kpl 2.4.18 -->
        <hl7fi:signatureCollection>
            <hl7fi:signature ID="OID1.2.246.10.77777777.10.0.91.2009.797.20210903.143727726">
                <hl7fi:signatureDescription code="4" codeSystem="1.2.246.537.5.40127.2006" codeSystemName="Sähköisen allekirjoituksen tyyppi" displayName="Järjestelmäallekirjoitus / KanTa"/>
                <hl7fi:signatureTimestamp ID="ID${headers.CamelFreemarkerDataModel.signatureTimestampAsId}">${headers.CamelFreemarkerDataModel.signatureTimestamp}</hl7fi:signatureTimestamp>
            </hl7fi:signature>
        </hl7fi:signatureCollection>
        <!-- FI  22	hl7fi:custodianTypecode - asiakirjan rekisteripitäjän laji -->
        <hl7fi:custodianTypeCode code="2" displayName="Yksityinen" codeSystem="1.2.246.537.5.40172.2008"
                                 codeSystemName="eArkisto - Rekisteripitäjän laji"/>
        <!-- FI  28	hl7fi:retentionPeriodClass - asiakirjan säilytysaikaluokka -->
        <!-- huom. tähän muutos ja tulee koodi 3, jos ok muuttaa LILMin säilytysaikaluokka -->
        <hl7fi:retentionPeriodClass code="2" displayName="12 vuotta potilaan kuolemasta tai 120 vuotta syntymästä"
                                    codeSystem="1.2.246.537.5.40158.2008"
                                    codeSystemName="eArkisto - Säilytysaikaluokka"/>
    </hl7fi:localHeader>
    <!--  End of local Header -->
    <!--
********************************************************
  CDA Body
********************************************************
-->
    <component>
        <!-- xml ID sähköistä allekirjoitusta varten -->
        <structuredBody ID="OID${headers.CamelFreemarkerDataModel.lilmDocId}.3">
            <!-- LILM lomake 0 -->
            <component>
                <section>
                    <!-- lomakkeen versio lomake OID ja päivityspäiväys vvvvkkpp-->
                    <templateId root="1.2.246.537.6.12.2002.322.20161001"/>
                    <!-- lomakkeen (instanssin, merkinnän) yksilöintitunnus -->
                    <id root="1.2.246.10.77777777.10.0.91.2009.797.3.1.100"/>
                    <!-- lomaketunnus näkymät luokituksesta -->
                    <code code="322" displayName="Luovutusilmoitusasiakirja" codeSystem="1.2.246.537.6.12.2002"
                          codeSystemName="AR/YDIN - Näkymät"/>
                    <!-- lomaketunnus näyttömuotoa varten titleen -->
                    <title>Luovutusilmoitusasiakirja</title>
                    <!-- Luovutusilmoitusasiakirja 0.1 -->
                    <component>
                        <section>
                            <code code="1" displayName="Luovutusilmoitusasiakirja"
                                  codeSystem="1.2.246.537.6.12.2002.322" codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutusilmoitusasiakirja</title>
                            <!-- Luovutusilmoitusasiakirja: Asiakirjan tunniste -->
                            <component>
                                <section>
                                    <code code="2" displayName="Asiakirjan yksilöivä tunniste"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Asiakirjan yksilöivä tunniste</title>
                                    <text>${headers.CamelFreemarkerDataModel.lilmDocId}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <!-- Samaa section-rakenteessa olevaa tietokentän tunnistetta ei tarvitse toistaa codessa, joten nullFlavor -->
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="II" root="${headers.CamelFreemarkerDataModel.lilmDocId}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                        </section>
                    </component>
                    <!-- Luovutusilmoitusasiakirja päättyy -->
                    <!-- Potilas/Asiakas 0.3 -->
                    <component>
                        <section>
                            <code code="3" displayName="Potilas/asiakas" codeSystem="1.2.246.537.6.12.2002.322"
                                  codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Potilas/asiakas</title>
                            <!-- Potilas/Asiakas: Tunnus -->
                            <component>
                                <section>
                                    <code code="4" displayName="Tunnus" codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Tunnus</title>
                                    <text>${headers.CamelFreemarkerDataModel.patientid}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="II" root="1.2.246.21"
                                                   extension="${headers.CamelFreemarkerDataModel.patientid}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Potilas/Asiakas: Suku- ja etunimet -->
                            <component>
                                <section>
                                    <code code="5" displayName="Suku- ja etunimet"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Suku- ja etunimet</title>
                                    <text>${headers.CamelFreemarkerDataModel.familyName}
                                        , ${headers.CamelFreemarkerDataModel.givenName}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="PN">
                                                <family>${headers.CamelFreemarkerDataModel.familyName}</family>
                                                <given qualifier="CL">${headers.CamelFreemarkerDataModel.givenName}</given>
                                                <given>${headers.CamelFreemarkerDataModel.givenName}</given>
                                            </value>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Potilas/Asiakas: Syntymäaika (ei pakollinen) -->
                            <component>
                                <section>
                                    <code code="6" displayName="Syntymäaika" codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Syntymäaika</title>
                                    <text>${headers.CamelFreemarkerDataModel.birthtime}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="TS" value="${headers.CamelFreemarkerDataModel.birthdate}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                        </section>
                    </component>
                    <!-- Potilas/Asiakas päättyy -->
                    <!-- Luovutuksen antaja 0.7 -->
                    <component>
                        <section>
                            <code code="7" displayName="Luovutuksen antaja" codeSystem="1.2.246.537.6.12.2002.322"
                                  codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutuksen antaja</title>
                            <!-- Luovutuksen antaja: Palveluntuottaja -->
                            <component>
                                <section>
                                    <code code="8" displayName="Palveluntuottaja "
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Palveluntuottaja </title>
                                    <text>${headers.CamelFreemarkerDataModel.srcServiceProviderId}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="II"
                                                   root="${headers.CamelFreemarkerDataModel.srcServiceProviderId}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutuksen antaja: Palveluntuottajan nimi -->
                            <component>
                                <section>
                                    <code code="40" displayName="Palveluntuottajan nimi"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Palveluntuottajan nimi</title>
                                    <text>${headers.CamelFreemarkerDataModel.srcServiceProviderName}</text>
                                    <!-- ST tietotyypille ei tarvitse entryä, pelkkä tietokentän arvo text-elementissä riittää -->
                                </section>
                            </component>
                            <!-- Luovutuksen antaja: Rekisterinpitäjä -->
                            <component>
                                <section>
                                    <code code="9" displayName="Rekisterinpitäjä" codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Rekisterinpitäjä</title>
                                    <text>${headers.CamelFreemarkerDataModel.srcRegistryKeeperId}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="II"
                                                   root="${headers.CamelFreemarkerDataModel.srcRegistryKeeperId}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutuksen antaja: Rekisteri (EP) -->
                            <component>
                                <section>
                                    <code code="10" displayName="Rekisteri" codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Rekisteri</title>
                                    <text>${headers.CamelFreemarkerDataModel.srcRegistryDisplayName}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="CV" code="${headers.CamelFreemarkerDataModel.srcRegistry}"
                                                   displayName="${headers.CamelFreemarkerDataModel.srcRegistryDisplayName}"
                                                   codeSystemName="KanTa-palvelut - Potilasasiakirjan rekisteritunnus"
                                                   codeSystem="1.2.246.537.5.40150.2009"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutetut asiakirjat listattuna -->
                            <#list headers.CamelFreemarkerDataModel.documentIds as documentId>
                                <component>
                                    <section>
                                        <code code="57" displayName="Luovutuksen kohde"
                                              codeSystem="1.2.246.537.6.12.2002.322"
                                              codeSystemName="Luovutusilmoitusasiakirja"/>
                                        <title>Luovutuksen kohde</title>
                                    </section>
                                </component>
                                <!-- Luovutetut asiakirjat: Asiakirjan tunniste -->
                                <component>
                                    <section>
                                        <code code="58" displayName="Asiakirjan tunniste"
                                              codeSystem="1.2.246.537.6.12.2002.322"
                                              codeSystemName="Luovutusilmoitusasiakirja"/>
                                        <title>Asiakirjan tunniste</title>
                                        <text>${documentId}</text>
                                        <entry>
                                            <observation classCode="COND" moodCode="EVN">
                                                <code nullFlavor="NA"/>
                                                <value xsi:type="II" root="${documentId}"/>
                                            </observation>
                                        </entry>
                                    </section>
                                </component>
                            </#list>
                        </section>
                    </component>
                    <!-- luovutuksen antaja päättyy -->
                    <!-- Luovutuksen saaja 0.12 -->
                    <component>
                        <section>
                            <code code="12" displayName="Luovutuksen saaja" codeSystem="1.2.246.537.6.12.2002.322"
                                  codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutuksen saaja</title>
                            <!-- Luovutuksen saaja: Luovutuksen saajan tunniste (EP) -->
                            <component>
                                <section>
                                    <code code="13" displayName="Luovutuksen saajan tunniste"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Luovutuksen saajan tunniste</title>
                                    <text>${headers.CamelFreemarkerDataModel.destServiceProviderId}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="II"
                                                   root="${headers.CamelFreemarkerDataModel.destServiceProviderId}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutuksen saaja: Luovutuksen saajan nimi -->
                            <component>
                                <section>
                                    <code code="42" displayName="Luovutuksen saajan nimi"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Luovutuksen saajan nimi</title>
                                    <text>${headers.CamelFreemarkerDataModel.destServiceProviderName}</text>
                                </section>
                            </component>
                            <!-- Luovutuksen saaja: Rekisterinpitäjä (EP)-->
                            <component>
                                <section>
                                    <code code="14" displayName="Rekisterinpitäjä"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Rekisterinpitäjä</title>
                                    <text>${headers.CamelFreemarkerDataModel.destRegistryKeeperId}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="II"
                                                   root="${headers.CamelFreemarkerDataModel.destRegistryKeeperId}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutuksen saaja: Rekisteri (EP)-->
                            <component>
                                <section>
                                    <code code="15" displayName="Rekisteri" codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Rekisteri</title>
                                    <text>${headers.CamelFreemarkerDataModel.destRegistryDisplayName}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="CV" code="${headers.CamelFreemarkerDataModel.destRegistry}"
                                                   displayName="${headers.CamelFreemarkerDataModel.destRegistryDisplayName}"
                                                   codeSystemName="KanTa-palvelut - Potilasasiakirjan rekisteritunnus"
                                                   codeSystem="1.2.246.537.5.40150.2009"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                        </section>
                    </component>
                    <!-- Luovutuksen saaja päättyy -->
                    <!-- Luovutuslokitietojen julkisuus 0.23 -->
                    <component>
                        <section>
                            <code code="23" displayName="Luovutuslokitietojen julkisuus"
                                  codeSystem="1.2.246.537.6.12.2002.322" codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutuslokitietojen julkisuus</title>
                            <!-- Luovutuslokitietojen julkisuus: Salassapitoaika/pysyvä -->
                            <component>
                                <section>
                                    <code code="24" displayName="Salassapitoaika/pysyvä"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Salassapitoaika/pysyvä</title>
                                    <text>${headers.CamelFreemarkerDataModel.dateNowAsDisplayDate}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="TS"
                                                   value="${headers.CamelFreemarkerDataModel.dateNowAsDate}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                        </section>
                    </component>
                    <!-- Luovutustietojen julkisuus päättyy -->
                    <!-- Luovutuksen perusteet 0.25 -->
                    <component>
                        <section>
                            <code code="25" displayName="Luovutuksen perusteet" codeSystem="1.2.246.537.6.12.2002.322"
                                  codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutuksen perusteet</title>
                            <!-- Luovutuksen perusteet: Luovutuksen peruste -->
                            <component>
                                <section>
                                    <code code="28" displayName="Luovutuksen peruste"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Luovutuksen peruste</title>
                                    <text>Luovutus terveyden- ja sairaanhoidon tarkoitukseen</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="CV" code="1"
                                                   displayName="Luovutus terveyden- ja sairaanhoidon tarkoitukseen"
                                                   codeSystemName="eArkisto - Luovutuksen peruste"
                                                   codeSystem="1.2.246.537.5.40166.2013"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                        </section>
                    </component>
                    <!-- Luovutuksen perusteet päättyy -->
                    <!-- Luovutustapa 0.53 -->
                    <component>
                        <section>
                            <code code="53" displayName="Luovutustapa" codeSystem="1.2.246.537.6.12.2002.322"
                                  codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutustapa</title>
                            <!-- Luovutustapa: Asiakirjojen luovutustapa -->
                            <component>
                                <section>
                                    <code code="54" displayName="Asiakirjojen luovutustapa"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Asiakirjojen luovutustapa</title>
                                    <text>Sähköinen, jonka palvelunantaja välittää</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="CV" code="2"
                                                   displayName="Sähköinen, jonka palvelunantaja välittää"
                                                   codeSystemName="eArkisto - Asiakirjan luovutustapa"
                                                   codeSystem="1.2.246.537.5.40165.2008"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                        </section>
                    </component>
                    <!-- Luovutuksen tekijä 0.33 -->
                    <component>
                        <section>
                            <code code="33" displayName="Luovutuksen tekijä" codeSystem="1.2.246.537.6.12.2002.322"
                                  codeSystemName="Luovutusilmoitusasiakirja"/>
                            <title>Luovutuksen tekijä</title>
                            <!-- Luovutuksen tekijä: Automaattiluovutus -->
                            <component>
                                <section>
                                    <code code="55" displayName="Automaattiluovutus"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Automaattiluovutus</title>
                                    <text>Kyllä</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="BL" value="true"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutuksen tekijä: Järjestelmän nimi -->
                            <component>
                                <section>
                                    <code code="56" displayName="Järjestelmän nimi"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Järjestelmän nimi</title>
                                    <text>Järjestelmä ZYX 1.12</text>
                                </section>
                            </component>
                            <!-- Luovutuksen tekijä: Luovutuksen ajankohta -->
                            <component>
                                <section>
                                    <code code="38" displayName="Luovutuksen ajankohta"
                                          codeSystem="1.2.246.537.6.12.2002.322"
                                          codeSystemName="Luovutusilmoitusasiakirja"/>
                                    <title>Luovutuksen ajankohta</title>
                                    <text>${headers.CamelFreemarkerDataModel.timeAsDisplayDateTime}</text>
                                    <entry>
                                        <observation classCode="COND" moodCode="EVN">
                                            <code nullFlavor="NA"/>
                                            <value xsi:type="TS"
                                                   value="${headers.CamelFreemarkerDataModel.timeAsDateTime}"/>
                                        </observation>
                                    </entry>
                                </section>
                            </component>
                            <!-- Luovutuksen ajankohta päättyy -->
                        </section>
                    </component>
                    <!-- Luovutuksen tekijä 0.33 päättyy -->
                </section>
            </component>
        </structuredBody>
    </component>
</ClinicalDocument>

