<#ftl output_format="XML" encoding="utf-8">
<?xml version="1.0" encoding="UTF-8"?>
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/">
    <soapenv:Header/>
    <soapenv:Body>
       ${headers.CamelFreemarkerDataModel.mr_message}
    </soapenv:Body>
</soapenv:Envelope>