<#ftl output_format="XML" encoding="utf-8">
<RCMR_IN100002FI01 ITSVersion="XML_1.0" xmlns="urn:hl7-org:v3">
	<id root="${headers.CamelFreemarkerDataModel.lilmDocId}.1" />
	<creationTime value="${headers.CamelFreemarkerDataModel.dateNowAsDateTime}" />
	<versionCode code="V3-2006" />
	<interactionId extension="RCMR_IN100002FI01" root="2.16.840.1.113883.1.6" />
	<profileId root="1.2.246.777.11.2019.10" />
	<processingCode code="D" />
	<processingModeCode code="T" />
	<acceptAckCode code="ER" />
	<receiver typeCode="RCV">
		<device classCode="DEV">
			<id root="${headers.CamelFreemarkerDataModel.receiverId}" />
		</device>
	</receiver>
	<sender typeCode="SND">
		<device classCode="DEV">
			<!-- sanoman lähettävä organisaatioyksikkö, yleensä palveluyksikkötasoinen organisaatioyksikkö -->
			<id root="${headers.CamelFreemarkerDataModel.srcServiceProviderId}" />
		</device>
	</sender>
	<web:controlActProcess moodCode="EVN" xmlns:web="urn:hl7-org:v3">
		<code code="RCMR_IN100002" codeSystem="2.16.840.1.113883.1.18" />
		<reasonCode code="PP32" codeSystem="1.2.246.537.5.40157.2008" />
		<authorOrPerformer typeCode="PRF">
			<assignedPerson>
				<id nullFlavor="NA" />
				<assignedPerson>
					<name nullFlavor="NA" />					
					<asLicensedEntity>
						<code code="1" codeSystem="1.2.246.537.5.40128.2006" displayName="Terveydenhuollon ammattihenkilön varmenne (TEO)" />
					</asLicensedEntity>
				</assignedPerson>
				<representedOrganization>
					<!-- viestin lähettänyt organisaatio eli palvelunantaja -->
					<id root="${headers.CamelFreemarkerDataModel.srcServiceProviderId}" />
					<name>${headers.CamelFreemarkerDataModel.srcServiceProviderName}</name>
				</representedOrganization>
			</assignedPerson>
		</authorOrPerformer>
		<subject>
			<clinicalDocument>
				<realmCode code="FI" />
				<typeId extension="POCD_HD000040" root="2.16.840.1.113883.1.3" />
				<id root="${headers.CamelFreemarkerDataModel.lilmDocId}" />
				<code code="13" codeSystem="1.2.246.537.5.40150.2009" codeSystemName="KanTa-palvelut - Potilasasiakirjan rekisteritunnus" displayName="Tiedonhallintapalvelun asiakirjat" />
				<text mediaType="multipart/related">${headers.CamelFreemarkerDataModel.cda_base64}</text>
				<statusCode code="completed" />
				<effectiveTime value="${headers.CamelFreemarkerDataModel.dateNowAsDateTime}" />
				<confidentialityCode code="5" codeSystem="1.2.246.777.5.99902.2006" codeSystemName="Kanta-palvelut - Asiakirjan luottamuksellisuus" displayName="Terveydenhuollon salassapidettävä" />
				<setId root="${headers.CamelFreemarkerDataModel.lilmDocId}" />
				<versionNumber value="1" />
				<recordTarget>
					<patient>
						<id extension="${headers.CamelFreemarkerDataModel.patientid}" root="1.2.246.21" />
						<statusCode code="normal" />
						<patientPerson>
							<name>
								<given>${headers.CamelFreemarkerDataModel.givenName}</given>
								<given qualifier="CL">${headers.CamelFreemarkerDataModel.givenName}</given>
								<family>${headers.CamelFreemarkerDataModel.familyName}</family>
							</name>
							<administrativeGenderCode nullFlavor="NA" />
							<birthTime value="${headers.CamelFreemarkerDataModel.birthdate}" />
						</patientPerson>
					</patient>
				</recordTarget>
				<author>
					<!-- mr-dokumentti sanoo että functionCode ei käytössä -->
					<functionCode code="HOIVAS" codeSystem="1.2.246.537.5.40006.2003" codeSystemName="HL7 - Lääkärin funktio/rooli 2003" displayName="Hoitovastuussa oleva ammattihenkilö" />
					<time nullFlavor="NA" />
					<assignedAuthor>
						<id nullFlavor="NA" />
						<assignedPerson nullFlavor="NA" />
					</assignedAuthor>
				</author>
				<custodian>
					<assignedCustodian>
						<representedOrganization>
							<id root="${headers.CamelFreemarkerDataModel.assignedCustodianId}" />
							<name>${headers.CamelFreemarkerDataModel.assignedCustodianName}</name>
						</representedOrganization>
					</assignedCustodian>
				</custodian>				
				<hl7fi:localHeader xmlns:hl7fi="urn:hl7finland" />
			</clinicalDocument>
		</subject>
	</web:controlActProcess>
</RCMR_IN100002FI01>